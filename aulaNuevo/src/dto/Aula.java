package dto;

public class Aula {
     
    //Aqu� creo las variables como atributo
    private int id;
    private Profesor profesor;
    private Estudiante[] alumno;
    private String materia;
     
    //Aqu� creo la variable como constante
    private final int ALUMNO=20;
     

    //Aqu� creamos un m�todo donde generamos un n�mero aleatorio
    public static int numeroAleatorio(int minimo, int maximo){
        
    	int num=(int)Math.floor(Math.random()*(minimo-(maximo+1))+(maximo+1));
        return num;
    }
    
    //Aqu� crear� un constructor donde le pondr� valor a las variables creadas anteriormente
    public Aula(){
      
        id= 1;
        profesor=new Profesor();
        alumno= new Estudiante[ALUMNO];
        creaAlumno();
        materia=Materia.MATERIAS_AULA[numeroAleatorio(0,2)];
         
    }
     
     
    //Aqu� creo un m�todo donde generar� la cantidad de alumno que tendr� la aula
    private void creaAlumno(){
         
        for(int i=0;i<alumno.length;i++){
        	
            alumno[i]=new Estudiante();
        }
         
    }
     
     

    //Aqu� hacemos un bucle tipo for donde sumamos un contador en el caso de que el total de contadores
    //sea igual a como m�nimo la mitad de la aula
    private boolean asistenciaAlumno(){
         
        int cuentaAsistencias=0;
         
        //contamos las asistencias
        for(int i=0;i<alumno.length;i++){
             
            if(alumno[i].isAsistencia()){
                cuentaAsistencias++;
            }
             
        }
        
        //Aqu� creo una condici�n donde me calcula que si el n�mero de contadores �s superior a la longitud de la lista de alumno
        // dividida entre dos (la mitad en total), que devuelva un true, de lo contrario, devolver� un false
        if (cuentaAsistencias >= ((int)(alumno.length/2))) { 
            return true; 
    } 
        else { 
             return false;
    }
         
    }
     
    
    public boolean darClase(){
         

    	//Aqu� creo una condici�n donde si el profesor no tiene asistencia en clase, no se podr� dar clase
        if(!profesor.isAsistencia()){
            System.out.println("El profesor no se encuentra disponible, hoy no hay clase");
            return false;
          
        }
        //Aqu� creo una condici�n donde si la materia del profesor no le pertenece a a la aula
        //no s� podr� dar clase
        else if(!profesor.getMateria().equals(materia)){
            System.out.println("La materia que ense�a el profesor y la aula no concuerdan, hoy no hay clase");
            return false;
        }
        //Aqu� creo una condici�n donde si la asisterncia de clase, no llega como m�nimo a la mitad, no se podr� dar clase
        else if (!asistenciaAlumno()){
            System.out.println("La asistencia no es suficiente, no se puede dar clase");
            return false;
        }
        //En el caso de que ninguna de las anteriores condiciones se cumplan, se podr� dar clase
        System.out.println("Hoy hay clase");
        return true;
         
    }
     

    //Aqu� creo un m�todo donde muestro la cantidad de chicos y chicas que han aprobado
    public void notas(){
        //Aqu� creo dos variables para ir sumando al cantidad de chicos y chicas
        int chicosAprobados=0;
        int chicasAprobadas=0;
         
         for(int i=0;i<alumno.length;i++){
            
           //Aqu� creo una condici�n que indica si el alumno est� aprobado o  no
           if(alumno[i].getNota()>=5){
               //Dependiendo de si es chico o chica, el contador correspondiente se sumar�
               if(alumno[i].getSexo()=='H'){
          	   
            	   chicosAprobados++;   
               }
               if (alumno[i].getSexo()=='M'){
            	   chicasAprobadas++;
               }
               
               //Aqu� mostramos el nombre del alumno
               System.out.println(alumno[i].toString());
                
           }
             
        }
         //Aqu� muestro la cantidad de chicos y chicas que han aprobado
        System.out.println("Hay "+ chicosAprobados + " chicos y " + chicasAprobadas + " chicas que han aprobado su asignaturas");
         
    }
     
}