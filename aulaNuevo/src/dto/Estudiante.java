package dto;

public class Estudiante extends Personal{
     
	//Aqu� creo la varaiable nota
    private int nota;
     
    //Aqu� creo un constructor donde le doy valor a la variable nota y indico la edad del alumno
    public Estudiante(){
        super();        
        nota= numeroAleatorio(0,10);
        super.setEdad(numeroAleatorio(12,15));
         
    }
 
     
    //Aqu� creo los getters y los setters de la variable nota 
    public int getNota() {
        return nota;
    }
 

    public void setNota(int nota) {
        this.nota = nota;
    }
 
   
    //Aqu� creo un m�todo void donde indico si se podr� o no hacer clase a trav�s de la asistencia de 
    //los alumnos y de los profesores
    public void disponible() {
         
        int dispoDelPersonal = numeroAleatorio(0, 100);
         
        //Aqu� creo una condic�n que si el personal �s inferior a 50 personas no podr� haber clase
        //de lo contrario, S�
        if(dispoDelPersonal<50){
            super.setAsistencia(false);  
        }
        else{
            super.setAsistencia(true);
        }
         
    }
     
    //Aqu� creo un toString para poder mostrar por consola los valores de las variables creadas
    public String toString(){
         
        return "Nombre: "+super.getNombre()+" ,sexo: "+super.getSexo()+" , nota: "+nota;
        
    }


   
}