package dto;

public class Profesor extends Personal {

	private String materia;
	
	public Profesor() {
		
		super.setEdad(numeroAleatorio(20, 70));
		
		materia = Materia.MATERIAS_AULA[numeroAleatorio(0, 2)];
		
	}
	
	public String getMateria() {
		return materia;
	}



	public void setMateria(String materia) {
		this.materia = materia;
	}

	public void disponible() {
		
		int porcentaje = numeroAleatorio(0, 100);
		
		if (porcentaje <= 20) {
			
			super.setAsistencia(false);
			
		} else {
			
			super.setAsistencia(true);
			
		}
		
	}

	
	
}
