package aulaNuevo;

import dto.Aula;

public class claseApp {

	public static void main(String[] args) {
		//Aqu� creo un objeto llamado aula
        Aula aulaDeClase=new Aula();
         
        //Aqu� analizo si se puede dar clase en la aula y muestra las notas de los alumnos
        if(aulaDeClase.darClase()){
        	aulaDeClase.notas();
        }

	}

}

